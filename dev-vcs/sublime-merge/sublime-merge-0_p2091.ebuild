# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg-utils

MV=${PV:0:1}
MY_PV=${PV#*_p}

DESCRIPTION="Git Client, done Sublime"
HOMEPAGE="https://www.sublimemerge.com/"
SRC_URI="https://download.sublimetext.com/sublime_merge_build_${MY_PV}_x64.tar.xz"

LICENSE="Sublime"
SLOT="0"
KEYWORDS="~amd64"
IUSE="dbus"
RESTRICT="bindist mirror strip"

QA_PREBUILT="*"
S="${WORKDIR}/sublime_merge"

RDEPEND="
	dev-libs/glib:2
	x11-libs/gtk+:3
	x11-libs/libX11
	dbus? ( sys-apps/dbus )"

src_install() {
	insinto /opt/sublime_merge
	doins -r Packages Icon
	doins changelog.txt
	exeinto /opt/sublime_merge
	doexe crash_handler git-credential-sublime ssh-askpass-sublime sublime_merge
	dosym ../../opt/sublime_merge/sublime_merge /usr/bin/smerge

	local size
	for size in 16 32 48 128 256; do
		dosym ../../../../../../opt/sublime_merge/Icon/${size}x${size}/sublime-merge.png \
			/usr/share/icons/hicolor/${size}x${size}/apps/smerge.png
	done

	make_desktop_entry "smerge" "Sublime Merge" "smerge" \
		"Development;" "StartupNotify=true"

	mv "${ED}"/usr/share/applications/smerge{-sublime-merge,}.desktop
}

pkg_postrm() {
	xdg_icon_cache_update
}

pkg_postinst() {
	xdg_icon_cache_update
}
