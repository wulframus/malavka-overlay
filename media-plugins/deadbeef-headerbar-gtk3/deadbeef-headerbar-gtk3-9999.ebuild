# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools git-r3

DESCRIPTION="Adds a headerbar to your DeaDBeeF"
HOMEPAGE="https://github.com/saivert/ddb_misc_headerbar_GTK3"
EGIT_REPO_URI="https://github.com/saivert/ddb_misc_headerbar_GTK3.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}
	media-sound/deadbeef
	x11-libs/gtk+:3"
BDEPEND=""

src_prepare() {
	eautoreconf
	eapply_user
}

src_install() {
	insinto "/usr/$(get_libdir)/deadbeef"
	insopts -m755
	doins src/.libs/ddb_misc_headerbar_GTK3.so
	dodoc README.md
}
