# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Playback Status Widget for the DeaDBeeF Audio Player"
HOMEPAGE="https://github.com/cboxdoerfer/ddb_playback_status"
EGIT_REPO_URI="https://github.com/cboxdoerfer/ddb_playback_status.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}
	media-sound/deadbeef
	x11-libs/gtk+:3"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${PN}-fix_format.patch"
)

src_compile() {
	emake gtk3
}

src_install() {
	insinto "/usr/$(get_libdir)/deadbeef"
	insopts -m755
	doins gtk3/ddb_misc_playback_status_GTK3.so
}
