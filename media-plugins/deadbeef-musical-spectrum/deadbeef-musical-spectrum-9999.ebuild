# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Musical spectrum for the DeaDBeeF audio player"
HOMEPAGE="https://github.com/cboxdoerfer/ddb_musical_spectrum"
EGIT_REPO_URI="https://github.com/cboxdoerfer/ddb_musical_spectrum.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}
	media-sound/deadbeef
	x11-libs/gtk+:3
	sci-libs/fftw"
BDEPEND=""

src_compile() {
	emake gtk3
}

src_install() {
	insinto "/usr/$(get_libdir)/deadbeef"
	insopts -m755
	doins gtk3/ddb_vis_musical_spectrum_GTK3.so
	einstalldocs
}
