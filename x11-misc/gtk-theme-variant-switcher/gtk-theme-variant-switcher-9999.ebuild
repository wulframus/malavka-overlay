# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8..12} )

inherit desktop gnome2-utils xdg-utils python-r1 git-r3

DESCRIPTION="Gtk Theme Variant Switcher"
HOMEPAGE="https://github.com/oktayacikalin/gtk-theme-variant-switcher"
EGIT_REPO_URI="https://github.com/oktayacikalin/gtk-theme-variant-switcher.git"

LICENSE="MIT"
SLOT="0"
IUSE=""

PATCHES=(
	"${FILESDIR}/${P}_desktop.patch"
)
DEPEND=""
RDEPEND="${DEPEND}
	${PYTHON_DEpS}
	x11-apps/xprop
	dev-python/pygobject"
BDEPEND=""

src_compile() {
	:
}

src_install() {
	newbin switcher.py ${PN}
	python_replicate_script "${D}"/usr/bin/${PN}
	dodir "/usr/share/glib-2.0/schemas"
	insinto "/usr/share/glib-2.0/schemas"
	newins "gschema.xml" "org.gtk.Settings.ThemeVariantSwitcher.gschema.xml"
	newicon "switcher.png" "${PN}.png"
	newmenu "switcher.desktop" "${PN}.desktop"
	dodoc "README.md"
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_desktop_database_update
}
