# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [ "${PV}" == "9999" ]; then
	VCS_ECLASS="git-r3"
fi

inherit cmake ${VCS_ECLASS}

DESCRIPTION="Linux multi-touch gesture recognizer"
HOMEPAGE="https://github.com/JoseExposito/touchegg"

if [ "${PV}" == "9999" ]; then
	EGIT_REPO_URI="https://github.com/JoseExposito/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/JoseExposito/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

RESTRICT="mirror"

LICENSE="GPL-3+"
SLOT="0"

IUSE="systemd"

DEPEND=""
RDEPEND="${DEPEND}
	dev-libs/libinput
	virtual/udev
	dev-libs/pugixml
	x11-libs/cairo
	x11-libs/libX11
	dev-libs/glib
	x11-libs/libXtst
	x11-libs/libXrandr
	systemd? ( sys-apps/systemd )
"
BDEPEND=""

DOCS=(
	"README.md"
	"CHANGELOG.md"
)

src_configure() {
	local mycmakeargs=(
		-DUSE_SYSTEMD=$(usex systemd)
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install
	newinitd "${FILESDIR}"/touchegg.initd touchegg
}
