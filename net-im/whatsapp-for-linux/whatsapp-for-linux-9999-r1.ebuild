# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [ "${PV}" == "9999" ]; then
	VCS_ECLASS="git-r3"
fi

inherit cmake ${VCS_ECLASS}

DESCRIPTION="An unofficial WhatsApp desktop application for Linux"
HOMEPAGE="https://github.com/eneshecan/whatsapp-for-linux"
if [ "${PV}" == "9999" ]; then
	KEYWORDS=""
	EGIT_REPO_URI="https://github.com/eneshecan/${PN}.git"
else
	KEYWORDS="~amd64 ~x86"
	SRC_URI="https://github.com/eneshecan/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="GPL-3"
SLOT="0"

RESTRICT="mirror"

DEPEND=""
RDEPEND="${DEPEND}
	net-libs/webkit-gtk:4
	dev-cpp/gtkmm:3.0
	dev-libs/libappindicator:3"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${PN}-1.4.1_libappindicator.patch"
)

src_prepare() {
	glib-compile-resources --generate-source --target="${S}/src/Resources.c" --sourcedir="${S}/resource" "${S}/resource/gresource.xml"
	eapply_user
	cmake_src_prepare
}
