# MALAVKA ![Build Status](http://gitlab.com/wulframus/malavka-overlay/badges/master/build.svg)

My [Gentoo Linux](https://gentoo.org) installation overlay.

All ebuilds written for **x86** and **amd64** architectures.

* * *

## app-portage/layman

To add our overlay via [layman](https://wiki.gentoo.org/wiki/Layman) just do:

```
layman -o https://gitlab.com/wulframus/malavka-overlay/raw/master/layman.xml -f -a malavka
```

* * *

## sys-apps/portage

Since version 2.2.16, Portage has a native mechanism for adding overlays.

Just copy `malavka.conf` to your `/etc/portage/repos.conf` directory:

```
mkdir -p /etc/portage/repos.conf
wget https://gitlab.com/wulframus/malavka-overlay/raw/master/repos.conf -O /etc/portage/repos.conf/malavka.conf
```

Edit the "path" variable in `/etc/portage/repos.conf/malavka.conf` to suit your overlay storage path
(for example, `/usr/local/portage/malavka`).
Now you can sync the overlay using `emaint sync -r malavka`.
